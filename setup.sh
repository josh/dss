#!/bin/bash
# Setup for determined-server-setup
# Written by Josh Mudge
# Ad Mejorem Dei Glorium

version=$(curl -s https://git.coolaj86.com/josh/dss/raw/branch/master/VERSION | cat)

echo "Installing dss $version"

curl -sO https://git.coolaj86.com/josh/dss/raw/branch/master/determined-server-setup.sh

sudo mv determined-server-setup.sh /usr/local/bin/dss

sudo chmod +x /usr/local/bin/dss

curl -sO https://git.coolaj86.com/josh/dss/raw/branch/master/harden-server.sh

sudo mv harden-server.sh /usr/local/bin/determined-harden-ssh

sudo chmod +x /usr/local/bin/determined-harden-ssh

echo "Done. Run 'dss' to use."
