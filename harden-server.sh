#!/bin/bash
# Determined SSH Hardening
# Written by Josh Mudge
# Ad Mejorem Dei Glorium

usr=$USER
version="v1.4.3 Alpha"
keyserver="git.coolaj86.com/josh/dss/raw/branch/master/"

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    setup)
    setup=1
    shift # past argument
    ;;
    --user)
    usr="$2"
    shift # past argument
    ;;
    --user2)
    user2="$2"
    shift # past argument
    ;;
    --user3)
    user3="$2"
    shift # past argument
    ;;
    --user4)
    user4="$2"
    shift # past argument
    ;;
    --user5)
    user5="$2"
    shift # past argument
    ;;
    -h|--help)
      echo determined-harden-ssh $version
      echo "Usage: determined-harden-ssh --user USERNAME"
      exit 1
    ;;
    *)
      # unknown option
      if [ -z "${user}" ]; then
        echo determined-harden-ssh $version
        echo "No admin user specified."
        echo "Usage: determined-harden-ssh --user USERNAME"
      else
        echo "unrecognized option '$1'"
        exit 1
      fi
    ;;
esac
shift # past argument or value
done

if test ! -z $usr
then

  echo "Installing fail2ban and hardening SSH configuration."
  # Install fail2ban
  sudo apt-get install -y fail2ban curl openssh-server > /dev/null

  echo "Creating new user by the username $usr"

  echo "Disabling password based logins in favor of SSH keys."

  # SSH keys only, no passwords.

  sudo sed -i "s/PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config
  sudo sed -i "s/#PasswordAuthentication no/PasswordAuthentication no/g" /etc/ssh/sshd_config
  sudo sed -i "s/PermitRootLogin yes/PermitRootLogin prohibit-password/g" /etc/ssh/sshd_config

  mkdir .tssh

  cd .tssh

  curl -sLO https://git.coolaj86.com/josh/dss/raw/branch/master/create-user.bash

  curl -sLO https://$keyserver/$usr.pub

  sudo mv create-user.bash /usr/local/bin/determined-create-user

  sudo chmod +x /usr/local/bin/determined-create-user

  if determined-create-user $usr;
  then
  echo "Setting up non-root admin user(s)"
  else
    echo "User creation failed. Please fix the above error and try again."
    cd ..
    rm -rf .tssh
    exit
  fi

  if test ! -z $user2
  then

    curl -sLO https://$keyserver/$user2.pub

    ./create-user.bash $user2

  fi

  if test ! -z $user3
  then

    curl -sLO https://$keyserver/$user3.pub

    ./create-user.bash $user3

  fi

  if test ! -z $user4
  then

    curl -sLO https://$keyserver/$user4.pub

    ./create-user.bash $user4

  fi

  if test ! -z $user5
  then

    curl -sLO https://$keyserver/$user5.pub

    ./create-user.bash $user5

  fi

  cd ..
  rm -rf .tssh

  echo "Disabling root login."

  sudo sed -i "s/PermitRootLogin prohibit-password/PermitRootLogin no/g" /etc/ssh/sshd_config
  sudo sed -i "s/PermitRootLogin without-password/PermitRootLogin no/g" /etc/ssh/sshd_config

  echo "That's it, we're done :)"

else

  echo determined-harden-ssh $version
  echo "No admin user specified."
  echo "Usage: ./harden-server.sh --user USERNAME"

fi
