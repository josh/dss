#!/bin/bash
# Josh's Automatic System Monitor
# Written by Josh Mudge
# Ad Mejorem Dei Glorium

update=1
version=v1.5.2a
alpha=0
dfh=$(df -h | grep '8[0-9]%')
dfh2=$(df -h | grep '9[0-9]%')

while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    --setup)
    shift # past argument
    setup=1
    ;;
    --no-update)
    update=0
    shift # past argument
    ;;
    --audit)
    audit=1
    shift # past argument
    ;;
    --email)
    email="$2"
    shift # past argument
    ;;
    -h|help)
    echo "dss-mon $version"
    echo "Usage: dss --monitor --email user@mailprovider.com"
    exit 1
    ;;
    -v|version)
    echo "dss $version"
    exit 1
    ;;
    *)
    # unknown option
    if test -z "${unknown}"
    then
      unknown=$1
    else
      echo "dss-mon $version"
      echo "Usage: dss --monitor --email user@mailprovider.com"
      exit 1
    fi
    ;;
  esac
  shift # past argument or value
done

if test $update = 1
then

  sudo apt-get update
  sudo apt-get upgrade
  sudo apt-get install sysstat # Check if installed, then do this
  curl -s "https://git.coolaj86.com/josh/dss/raw/branch/master/setup.sh" | bash

fi

# Cleanup

sudo apt-get clean

# Security Audit (Tackled by dss init before setting this up.)

# if test ! -f /home/.dssv1.7
# then
#
#   dss init
#
# fi

auth=$(sudo cat /var/log/auth.log | grep "Invalid user")
#auth2=$(sudo cat /var/log/auth.log | grep "Connection closed")

if test $alpha = 1;
then

  sudo apt-get autoremove

fi

# To setup email, point a domain name to your server using DNS.
# Disable any firewall rules that block port 25 (You may have to go to a server admin panel or contact your system administrator)
# Then run: sudo apt-get install mailutils
# Open up /etc/hosts and make sure it has:
# 127.0.1.1 mydomain.com myserverHOSTNAME
# Select "Internet Site" and enter the domain you want it to send email from.
# Then you can send email like this: echo "Body of email" | mail -s "subject" EMAILADDRESS

if test ! -z "$auth" # If set to run automatically, don't run this check every time.
then
  echo "Attacks found. Sending authentication log to $email"
  sudo cat /var/log/auth.log | grep "Invalid user" | mail -s "Invalid User Login" $email
fi

if test ! -z "$dfh"
then
  echo "Disk usage is high, sending disk usage to $email"
  echo "$dfh" | mail -s "High Disk Usage" $email
fi

if test ! -z "$dfh2"
then
  echo "Disk usage is critical, sending disk usage to $email"
  echo "$dfh2" | mail -s "Critical Disk Usage" $email
fi

for i in {1..300} # Do this 300 times.
do
CPU=$(mpstat 1 1 | awk '$3 ~ /CPU/ { for(i=1;i<=NF;i++) { if ($i ~ /%idle/) field=i } } $3 ~ /all/ { printf("%d",100 - $field) }') # Find CPU usage for the last 10 seconds. Code credit: Stackoverflow
CPUT=$(($CPUT + $CPU)) # Add each 1 second record to the total.
done
CPURESULT=$(($CPUT / 300)) # Divide the total by 300 seconds to find average CPU usage over the last 5 minutes.


if test $CPURESULT > 90
then
  echo "CPU usage is quite high, sending report to $email"
  echo "$CPURESULT %" | mail -s "High CPU Usage" $email
fi

USEDRAM=$(free | grep Mem | awk '{print ($2 -$7) / $2 * 100.0}')

if test $USEDRAM > 80
then
  echo "RAM usage is quite high, sending report to $email"
  echo "$USEDRAM %" | mail -s "High RAM Usage" $email
fi
