# determined-server-setup (dss)

determined-server-setup is a script that installs needed utilities/software on servers so you don't need to.

# Requirements

# Installation

You can install it by running:

`curl -s "https://git.coolaj86.com/josh/dss/raw/branch/master/setup.sh" | bash`

# Usage

This script is in the ALPHA stage. Use at your own risk.
```
dss --init # Update your server and install server utilities, setup automatic updates and harden SSH.
dss --clean  # Update the server and cleanup unneeded files and programs. Use with caution.
dss --log # Print the system log.`
dss --authlog 1 # Print the SSH authentication log. Use 'dss authlog attacks' to show attacks on your SSH server.
dss --user USERNAME init   # Setup server with server utilities and enable automatic security updates.
```
You can run: `dss help` for a list of all commands.

# Automatic Updates

When prompted to setup automatic updates, hit "yes" and when prompted with a text box, replace all references to "Debian" with the name of your distro. If you're running Ubuntu, you should replace all references of Debian with Ubuntu.
