#!/bin/bash
# determined-server-setup (dss)
# Written by Josh Mudge
# Ad Mejorem Dei Glorium

version=$(curl -s https://git.coolaj86.com/josh/dss/raw/branch/master/VERSION | cat)

# Get options from CLI arguments

usr=$USER
init=0
clean=0
log=0
authlog=0
update=0
mon=0

while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    --init)
    init=1
    shift # past argument
    ;;
    --clean)
    clean=1
    shift # past argument
    ;;
    --log)
    log=1
    shift # past argument
    ;;
    --authlog)
    authlog="$2"
    shift # past argument
    ;;
    --user)
    usr="$2"
    shift # past argument
    ;;
    --user2)
    user2="$2"
    shift # past argument
    ;;
    --user3)
    user3="$2"
    shift # past argument
    ;;
    --update)
    update=1
    shift # past argument
    ;;
    --monitor)
    mon=1
    shift # past argument
    ;;
    --mon-setup)
    mon=2
    shift # past argument
    ;;
    --email)
    email=1
    shift # past argument
    ;;
    --logfile)
    logfile=1
    shift # past argument
    ;;
    blacklist)
    blacklist="$2"
    shift # past argument
    ;;
    -h|help)
    echo "dss $version"
    echo "Usage: dss [OPTION]"
    echo "You can run the following commands:"
    echo "dss --clean  # Update the server and cleanup uneeded files and programs. Use with caution."
    echo "dss --log # Print the system log."
    echo "dss --authlog 1 # Print the SSH authentication log. Use 'dss authlog attacks' to show attacks on your SSH server."
    echo "dss --user USERNAME --init   # Setup server with server utilities and enable automatic security updates."
    exit 1
    ;;
    -v|version)
    echo "dss $version"
    exit 1
    ;;
    *)
    # unknown option
    if test -z "${unknown}"
    then
      unknown=$1
    else
      echo "dss $version"
      echo "dss --user USERNAME --init   # Setup server with server utilities and enable automatic security updates."
      exit 1
    fi
    ;;
  esac
  shift # past argument or value
done

if test $init = 1
then
  # Update server
  sudo apt-get update
  sudo apt-get upgrade -y

  # Install server utilities
  sudo apt-get install -y screen curl nano htop fail2ban rsync man shellcheck git software-properties-common

  # Prompt user to set up automatic security updates.
  sudo apt-get install -y unattended-upgrades
  sudo dpkg-reconfigure -plow unattended-upgrades

  # Harden ssh
  if determined-harden-ssh --user $usr
  then
    echo "dss" | sudo tee /home/.dssv1.7
  else
    "You cannot create root user and disable root login, that won't work... See 'dss help'"
    exit
  fi

elif test $log = 1
then

  sudo cat /var/log/syslog

elif test $authlog = 1
  then
    sudo cat /var/log/auth.log

elif test $authlog = attacks
  then
    sudo cat /var/log/auth.log | grep "Invalid user"
    sudo cat /var/log/auth.log | grep "Connection closed"
    exit

elif test ! -z $blacklist
then
  echo "Note to self: add blacklist function, empty elif is not allowed in BASH."
  # Blacklist code

elif test $update = 1
then
  # Update Linux and determined-setup
  sudo apt-get update
  sudo apt-get upgrade
  curl -s "https://git.coolaj86.com/josh/raw/master/dss/setup.sh" | bash

elif test $clean = 1
then
  # Update
  sudo apt-get update
  sudo apt-get upgrade

  # Cleanup
  sudo apt-get clean
  sudo apt-get autoremove

elif test $mon = 1
then

  cd /home
  ./sysmon.sh -- email $email

elif test $mon = 2
then

  dss init
  curl -sO "https://git.coolaj86.com/josh/raw/master/dss/sysmon.sh"
  sudo mv sysmon.sh /home/.sysmon.sh
 ( sudo crontab -l ; echo "14 1 * * * /bin/bash -c "/home/.sysmon.sh --email $email"" &> "$logfile" ) | sudo crontab -

else
  echo "dss $version"
  echo "Usage: dss [OPTION]"
  echo "You can run the following commands:"
  echo "dss --clean  # Update the server and cleanup uneeded files and programs. Use with caution."
  echo "dss --log # Print the system log."
  echo "dss --authlog 1 # Print the SSH authentication log. Use 'dss authlog attacks' to show attacks on your SSH server."
  echo "dss --user USERNAME init   # Setup server with server utilities and enable automatic security updates."
  exit 1
fi
